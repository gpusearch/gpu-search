import sqlite3 as sql
from sets import Set

DEFAULT_DB_PATH = 'data/buckets.db'

def table_exists(path=DEFAULT_DB_PATH):
	conn = sql.connect(path)
	cursor = conn.cursor()
	cursor.execute('''SELECT name FROM sqlite_master
			  WHERE type = 'table' AND name = 'buckets'
		       ''')
	tables = []
	for row in cursor:
		tables.append(row[0])
	conn.close()
	if not tables:
                return False
	return True

# The schema of the table:
# smi - the smiles id of this record
# RDK5FPhex - rdkit hex id for this record
# value - the hash value by lsh hash function
# bucket - which bucket does this record belong to
# key - the key for lsh hash function
# band_idx - which band does this bucket belongs to
# pre_comp - stores any pre-computed data.

def create_table(path=DEFAULT_DB_PATH):
	if table_exists(path):
		return
	conn = sql.connect(path)
	cursor = conn.cursor()
	cursor.execute('''CREATE TABLE buckets
		  	  (smi text,
                           RDK5FPhex text,
                           value integer,
			   bucket integer,
                           key integer,
			   band_idx integer,
			   pre_comp text)
		       ''')
	conn.commit()
	conn.close()

def insert_one(smi, rdk, value, bucket, key, band, pre, conn=None):
        no_conn = conn is None
        if no_conn:
                conn = sql.connect(DEFAULT_DB_PATH)
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO buckets VALUES (?, ?, ?, ?, ?, ?, ?)''', (smi, rdk, value, bucket, key, band, pre))
        if no_conn:
                conn.commit()
                conn.close()

def insert_one_sig(sig, conn=None):
        no_conn = conn is None
        if no_conn:
                conn = sql.connect(DEFAULT_DB_PATH)
	cursor = conn.cursor()
	cursor.execute('''INSERT INTO buckets VALUES (?, ?, ?, ?, ?, ?, ?)''', sig)
        if no_conn:
                conn.commit()
                conn.close()

def insert_multiple(band_seg, conn=None):
        no_conn = conn is None
        if no_conn:
                conn = sql.connect(DEFAULT_DB_PATH)
	cursor = conn.cursor()
	cursor = conn.executemany('''INSERT INTO buckets VALUES (?, ?, ?, ?, ?, ?, ?)''', band_seg)
        if no_conn:
                conn.commit()
                conn.close()


def display_bucket_content(key, band, bucket, conn=None):
        no_conn = conn is None
        if no_conn:
                conn = sql.connect(DEFAULT_DB_PATH)
	cursor = conn.cursor()
	cursor.execute('''SELECT B.smi FROM buckets B
			  where B.key  = (?) and B.band_idx = (?)
              and B.bucket = (?)''', (key,band,bucket))
        bucket = [row for row in cursor]


        if no_conn:
                conn.close()

	return bucket

def get_SMI(band, hash_value, path=DEFAULT_DB_PATH):
	conn = sql.connect(path)
	cursor = conn.cursor()
	cursor.execute('''SELECT * FROM buckets B
			  where B.value = (?) and B.band_idx = (?)''',
                       (hash_value, band))

        candidates = [row for row in cursor]

	conn.close()

	return candidates


def display_all(path=DEFAULT_DB_PATH):
        try:
                open(path, 'r')
        except:
                return []
	conn = sql.connect(path)
	cursor = conn.cursor()
        total = []
	for row in cursor.execute('''SELECT * FROM buckets'''):
		print row
                total.append(row)
	conn.close()
        return total

def get_count(path=DEFAULT_DB_PATH):
        try:
                open(path, 'r')
        except:
                return []
        conn = sql.connect(path)
        cursor = conn.cursor()
        cursor.execute('''SELECT count(*) FROM buckets''')
        count = cursor.fetchone()[0]
        conn.close()
        return count

def num_finished_molecules(path, num_hash_functions, rows_per_band,
                           **kwargs):
        row_count = get_count(path)
        bands = num_hash_functions/rows_per_band
        return row_count/bands

def main():
	create_table()
	#s1 = ["1234", 1234, 1, 1, "CGA", "CGA"]
	#insert_one_sig(s1)
	#l =  display_bucket_content("1234", 1)
	#print display_bucket_content("1234", 1)
	#insert_multiple(l)
	#print display_bucket_content("1234", 1)
	print list(get_SMI("1234", 1))
	print list(get_SMI("789", 3))

if __name__ == '__main__':
	main()
