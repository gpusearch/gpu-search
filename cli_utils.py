import argparse
import sys
import os
import sqlite3 as sql

import pandas as pd
from rdkit import Chem

import buckets

def confirm(prompt, validator='y', lower=True):
    """
    Gets user response and aborts execution if it doesn't match validator.
    converts input to lower if lower is True
    """
    response = raw_input(prompt)
    if lower:
        response = response.lower()

    if response != validator:
        print('Aborting')
        exit()


def drop_stderr():
    #save the current stderr
    stderr_fileno = sys.stderr.fileno()
    stderr_save = os.dup(stderr_fileno)

    #redirect errors to devnull
    devnull_fd = os.open(os.devnull, os.O_WRONLY)
    os.dup2(devnull_fd, stderr_fileno)

    #return descriptors necessary to restore stderr
    return stderr_fileno, stderr_save, devnull_fd


def recover_stderr(stderr_fileno, stderr_save, devnull_fd):
    #close devnull
    os.close(devnull_fd)
    #restore saved descriptor to stderr
    os.dup2(stderr_save, stderr_fileno)


def check_smiles_string(val):
    saved = None
    try:
        saved = drop_stderr()
        #MolFromSmiles only throws messages to stderr not exceptions
        #redirection required to hide messages but still type check
        molecule = Chem.MolFromSmiles(val)
        Chem.RDKFingerprint(molecule, maxPath=5, fpSize=1024)

        recover_stderr(*saved)
    except:
        if saved:
            recover_stderr(*saved)
        raise argparse.ArgumentTypeError("invalid smiles string: '%s'" % val)

    return val


def check_db_exists(val):
    try:
        if not buckets.table_exists(val):
            raise argparse.ArgumentTypeError("invalid target path: '%s'" % val)
    except:
        raise argparse.ArgumentTypeError("invalid target path: '%s'" % val)

    return val


def check_non_negative_int(val):
    #Ensures a value must be a non-negative int
    fail_flag = False
    ivalue = -1
    try:
        ivalue = int(val)
    except:
        fail_flag = True

    if fail_flag or (ivalue < 0):
        raise argparse.ArgumentTypeError("invalid non-negative int value: '%s'" % val)

    return ivalue

def check_positive_int(val):
    #Ensures a value must be a non-negative int
    fail_flag = False
    ivalue = -1
    try:
        ivalue = int(val)
    except:
        fail_flag = True

    if fail_flag or (ivalue < 1):
        raise argparse.ArgumentTypeError("invalid non-negative int value: '%s'" % val)

    return ivalue


def check_zero_to_one_float(val):
    #Ensure a value must be a float between 0 and 1
    fail_flag = False
    ivalue = -1
    try:
        ivalue = float(val)
    except:
        fail_flag = True

    if fail_flag or (ivalue < 0 or ivalue > 1):
        raise argparse.ArgumentTypeError("invalid 0-1 float value '%s'" % val)
    return ivalue

def check_valid_hdf5_store(val):
    necessary_columns = ['Smiles', 'RDK5FPhex']
    store = None
    try:
        store = pd.HDFStore(val, 'r')
        data = store.select('data')
        store.close()
        actual_columns = list(data.columns.values)
        for a, b in zip(necessary_columns, actual_columns):
            if a is b:
                raise Exception()
    except:
        if store:
            store.close()
        raise argparse.ArgumentTypeError("missing or incorrectly formated store: %s" % val)

    return val

def check_valid_shingle_size_int(val):
    #Ensures a value must be a non-negative int
    fail_flag = False
    ivalue = -1
    try:
        ivalue = int(val)
    except:
        fail_flag = True

    if fail_flag or (ivalue < 1 or ivalue > 1024):
        raise argparse.ArgumentTypeError("invalid 1-1024 int value: '%s'" % val)

    return ivalue

def confirm_args(args):
    dictionary = vars(args)
    for arg in dictionary:
        print('%s: %s' % (arg, dictionary[arg]))

def db_warning_for_precomp(path):
    try:
        with open(path, 'r') as f:
            pass
    except:
        print("Database does not exist. Creating file...")
        #confirm("Database does not exist. create file? (y/N): ")
        return

    if buckets.table_exists(path):
        print('Database has existing precomputation data, continuing precomputation...')

def check_invalid_hash_row_values(args):
    if args.num_hash_functions % args.rows_per_band is not 0:
        raise argparse.ArgumentTypeError('Total hash functions not divisible by rows per band')
