import os
import sys
sys.path.append('/data/gpusearch/lsh')

import lsh_search
import lsh_similarity

def main():
    print('\nGPU SEARCH')

    txt = raw_input("Enter 'g' for guided demo search, enter 'u' for unguided search.\n")
    
    if txt == 'g':
        print('To cancel demo at any time, press Ctrl+Z')
        print('Search begin on CN1C2C1(C)CC(CN)C2(C)CN')
        lsh_similarity.main()
    if txt == 'u':
        string = raw_input("Enter a search.\nFormat example: -m 100 -s 0.75 -b 24 -n 6 -r 3 -f -v -p 'CN1C2C1(C)CC(CN)C2(C)CN' 'data/buckets.db'\n")
        os.system('python ./lsh_search.py %s'%string)

if __name__ == '__main__':
    main()
