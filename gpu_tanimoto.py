import numpy as np
import time
import sys
import pandas as pd
import bitstring, itertools
from operator import itemgetter
from pycuda.compiler import SourceModule
import pycuda.driver as drv
import pycuda.autoinit

#data = '/data/gpusearch/data/example50K-hex.h5'

cuda_popcount = SourceModule("""
  __device__ float similarity(unsigned long long *query,
                              unsigned long long *target, int data_len,
                              float cutoff) {
    int a = 0, b = 0, c = 0;
    float result;
    // iterate over the length of the query and target arrays
    for (int i = 0; i < data_len; i++) {
      // use CUDA specific popcount function to do the comparison
      a += __popcll(query[i]);
      b += __popcll(target[i]);
      c += __popcll(query[i] & target[i]);
    }
    // Handle the edge case where denominator is zero
    if (a + b == c) {
      result = 1.0f;
    }
    // else do the normal computation for Tanimoto similarity
    else {
      result = (float) c / (a + b - c);
    }
    // check if the result is less than the cutoff
    if (result < cutoff) {
      result = 0;
    }
    // finally, return the result
    return result;
  }
  __global__ void tanimoto_popcount(unsigned long long *query, int query_len,
                                    unsigned long long *target, int target_len,
                                    int data_len, float cutoff, float *out) {
    // Compute which indices in the output matrix we should operate on.
    // These parameters are set on kernel launch
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int idy = blockDim.y * blockIdx.y + threadIdx.y;
    // if we have a valid index, do the computation
    if (idy < query_len && idx < target_len) {
      // compute our location in the output array and do the comparison
      out[idy * target_len + idx] =
      similarity(&query[idy * data_len], &target[idx * data_len], data_len,
                 cutoff);
    }
  }
""")

# Convert a bitstring to uint64
def bin2int64(x):
    return np.uint64(bitstring.BitArray(bin=x).uint)

# Convert binary fingerprint to list of uint64
def fp2int64(x):
    y = map(''.join, zip(*[iter(x)] * 64))
    return map(bin2int64, y)

# Convert a hexstring to uint64
def hex2int64(x):
    return np.uint64(bitstring.BitArray(hex=x).uint)

# Convert hexidemical fingerprint to list of uint64
def fphex2int64(x):
    y = map(''.join, zip(*[iter(x)] * 16))
    return map(hex2int64, y)

def getSim(item):
    return item[1]

# Display the similar molecules, sorted by the descedning order of similarity
def sort_display(res, SMI_list, num_cutoff):
    sims = []
    for row in res:
        sims.append(row[0])
    pair_list = zip(SMI_list, sims)
    display = []
    index = 1;
    for row in pair_list:
        if (row[1] != 0):
            display.append(row)
    if (len(display) == 0):
        print "No similar molecule found in the data base that satisfies your minimum similarity threshold. Consider setting it lower"
        return
    display.sort(key = getSim, reverse = True)
    final_display = []
    aux = []
    aux.append(("Index", "SMILES", "Similarity"))
    index = 1
    num = 0
    print "\nHere are the similar molecules:"
    for row in aux:
        print('{:>5}   {:<35} {:<15}'.format(*row))
    for row in display:
        if num < num_cutoff:
            final_display.append((index, row[0], row[1]))
            index = index + 1
            num = num + 1
        else:
            break
    for row in final_display:
        print('{:>5}   {:<35} {:.3f}'.format(*row))
    print "\n"
    return final_display

"""
    GPUtanimoto initializes and prepare the data in the right form for the CUDA tanimoto computation.
    Input:
	-query is the fingerprint of the single molecule of interest in bit-vector form
	-candidates is a list of 2-tuples whose first component is the SMI and second component is the fingerprint (we will need to covert it to bit-vector form below)
	-cutoff is the mininum similarity threshold
"""

def GPUtanimoto(query, candidates, num_cutoff, sim_cutoff):
    if (len(candidates) == 0):
        print "No similar molecule found in the data base"
        return
    SMI_list = []
    FP_list = []
    FP_list_query = []
    for c in candidates:
        SMI_list.append(c[0])
        FP_list.append(c[1])
    FP_list_query.append(query)
    target_fp = np.empty((len(FP_list), 16), dtype=np.uint64)
    target_fp = map(fphex2int64, FP_list)
    target_list = list(itertools.chain.from_iterable(target_fp))
    merged_target = np.reshape(target_list, (len(FP_list), 16))
    query_fp = np.empty((len(FP_list_query), 16), dtype=np.uint64)
    query_fp = map(fp2int64, FP_list_query)
    query_list = list(itertools.chain.from_iterable(query_fp))
    merged_query = np.reshape(query_list, (len(FP_list_query), 16))
    res = CUDA_search(merged_query, merged_target, sim_cutoff)
    display = sort_display(res, SMI_list, num_cutoff)
    #lsh_accuracy(merged_query, display)

def lsh_accuracy(merged_query, display):
    print "Performing a similar molecule search without lsh:"
    display_len = len(display)
    store = pd.HDFStore(data, )
    target = store.select('data', start = 0, stop = 50000)
    store.close()
    target_fp = np.empty((50000, 16), dtype = np.uint64)
    target_fp = target['RDK5FPhex'].apply(lambda x: fphex2int64(x))
    target_list = list(itertools.chain.from_iterable(target_fp.values))
    merged_target = np.reshape(target_list, (50000, 16))
    SMI = target['Smiles']
    SMI_list = []
    for row in SMI:
        SMI_list.append(row)
    res = CUDA_search(merged_query, merged_target, 0)
    sort_display(res, SMI_list, display_len)


def CUDA_search(query, target, cutoff):

    # Make sure that the inputs are properly formatted
    if len(query) == 0 or len(target) == 0:
        raise ValueError("Error, input must be of nonzero length.")

    # We need to properly format the input as uint64 matrices
    #print "Converting inputs to np.uint64s"
    query = query.astype(np.uint64)
    target = target.astype(np.uint64)

    # Get a reference to our CUDA function
    tanimoto = cuda_popcount.get_function("tanimoto_popcount")

    # CUDA kernel size parameters
    query_size = len(query)
    target_size = len(target)
    threads_per_block = 32  # constant dependent on hardware
    # Output array
    dest_in = np.zeros((target_size, query_size), dtype = np.float32)
    # Loop, reducing memory size until we can fit the target_idxob on the GPU
    not_enough_memory = True
    while not_enough_memory:
        # Determine the block and grid sizes
        dx, mx = divmod(len(target), threads_per_block)
        dy, my = divmod(len(query), threads_per_block)
        bdim = (threads_per_block, threads_per_block, 1)
        gdim = ((dx + (mx > 0)), (dy + (my > 0)))
        # Call the CUDA
        try:
            target_idx = 0
            query_idx = 0
            if (target_idx + target_size > len(target)):
                target_in = target[target_idx:len(target)]
            else:
                target_in = target[target_idx:target_idx + target_size]

            if (query_idx + query_size > len(query)):
                query_in = query[query_idx:len(query)]
            else:
                query_in = query[query_idx:query_idx + query_size]

            tanimoto(drv.In(query_in), np.int32(len(query_in)),
                     drv.In(target_in), np.int32(len(target_in)),
                     np.int32(len(query_in[0])), np.float32(cutoff), drv.Out(dest_in),
                     block=bdim, grid=gdim)
            not_enough_memory = False
	# If not enough memory, do something. However, highly unlikely for this case
	except pycuda._driver.MemoryError:
	    if (target_size > 1):
	        target_size = target_size / 2
	    else:
	        query_size = query_size / 2
		if (query_size == 0):
		    raise MemoryError("Unable to allocate memory.")
    return dest_in
