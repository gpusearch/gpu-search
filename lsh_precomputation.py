import argparse

import lsh_similarity
import cli_utils
from native_shingle import SHINGLE_SIZE
from native_min_hash import NUM_HASH_FUNCTIONS, NUM_SHINGLES
from native_lsh import ROWS_PER_BAND


def parser_warnings(args):
    if args.force:
        return

    if args.num_hash_functions > NUM_HASH_FUNCTIONS:
        cli_utils.confirm('Number of hash functions specified may cause extremely long computation time.\ncontinue? (y/N): ')

    if args.shingle_size < 15:
        cli_utils.confirm('Specified shingle size may reduce the effectiveness of the LSH algorithm.\ncontinue? (y/N): ')

    if args.shingle_size > SHINGLE_SIZE:
        cli_utils.confirm('Specified shingle size may cause extremely long computation time.\ncontinue? (y/N): ')

    cli_utils.db_warning_for_precomp(args.target)


def parse_args():
    descrip = """Precomputation for comparison based on LSH algorithms"""
    epi = """
    If the sqlite3 database does not exist it will be created.
    See the design document for more information on choosing parameters.
    """
    parser = argparse.ArgumentParser(description=descrip, epilog=epi)
    parser.add_argument('store', type=cli_utils.check_valid_hdf5_store,
                        help='hdf5 store containing smiles/fingerprint values')
    parser.add_argument('target', type=str,
                        help='sqlite3 db path to add precomputation info to')
    parser.add_argument('-b', default=SHINGLE_SIZE,
                        type=cli_utils.check_valid_shingle_size_int,
                        help='number of bits per shingle',
                        metavar='shingle_size', dest='shingle_size')
    parser.add_argument('-n', default=NUM_HASH_FUNCTIONS,
                        type=cli_utils.check_positive_int,
                        help='number of hash functions in total',
                        metavar='num_hashes', dest='num_hash_functions')
    parser.add_argument('-r', default=ROWS_PER_BAND,
                        type=cli_utils.check_positive_int,
                        help='number of hash functions per band',
                        metavar='num_rows', dest='rows_per_band')
    parser.add_argument('-c', default=lsh_similarity.CHUNK_SIZE,
                        type=cli_utils.check_positive_int,
                        help='size of chunks to do computation on',
                        metavar='chunk_size', dest='chunk_size')
    parser.add_argument('-f', '--force', action='store_true',
                        help='suppress warning messages')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='display progress messages')

    args = parser.parse_args()
    cli_utils.check_invalid_hash_row_values(args)
    parser_warnings(args)
    if args.verbose:
        cli_utils.confirm_args(args)
    return args

def main():
    args = parse_args()

    lsh_similarity.precompute_tanimoto_lsh(**vars(args))

if __name__ == '__main__':
    main()
