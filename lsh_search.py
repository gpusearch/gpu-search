import argparse

from rdkit import Chem

import cli_utils
import lsh_similarity
import buckets
from native_shingle import SHINGLE_SIZE
from native_min_hash import NUM_HASH_FUNCTIONS, NUM_SHINGLES
from native_lsh import ROWS_PER_BAND

#Example smiles string: CN1C2C1(C)CC(CN)C2(C)CN

DEFAULT_MAX_NUMBER_CUTOFF = lsh_similarity.DEFAULT_MAX_NUMBER_CUTOFF
DEFAULT_SIMILARITY_CUTOFF = lsh_similarity.DEFAULT_SIMILARITY_CUTOFF


def no_cutoff_warning():
    prompt = """No number or similarity cutoff specified. continue? (y/N): """
    cli_utils.confirm(prompt)

def parser_warnings(args):
    if args.force:
        return
    if args.num_cutoff is None and args.sim_cutoff is None:
        no_cutoff_warning()

def arg_defaults(args):
    if args.num_cutoff is None:
        args.num_cutoff = DEFAULT_MAX_NUMBER_CUTOFF
    if args.sim_cutoff is None:
        args.sim_cutoff = DEFAULT_SIMILARITY_CUTOFF

def parse_args():
    descrip = """Molecular comparison based on LSH algorithms"""
    parser = argparse.ArgumentParser(description=descrip)

    parser.add_argument('molecule', type=cli_utils.check_smiles_string,
                        help='smiles representation of a molecule query')
    parser.add_argument('target', type=cli_utils.check_db_exists,
                        help='sqlite3 db containing precomputed LSH information')
    parser.add_argument('-m', default=None,
                        type=cli_utils.check_non_negative_int,
                        help='maximum number of molecules returned',
                        metavar='max_returned', dest='num_cutoff')
    parser.add_argument('-s', default=None,
                        type=cli_utils.check_zero_to_one_float,
                        help='similarity cutoff for returned  molecules',
                        metavar='similarity', dest='sim_cutoff')
    parser.add_argument('-b', default=SHINGLE_SIZE,
                        type=cli_utils.check_valid_shingle_size_int,
                        help='number of bits per shingle',
                        metavar='shingle_size', dest='shingle_size')
    parser.add_argument('-n', default=NUM_HASH_FUNCTIONS,
                        type=cli_utils.check_positive_int,
                        help='number of hash functions in total',
                        metavar='num_hashes', dest='num_hash_functions')
    parser.add_argument('-r', default=ROWS_PER_BAND,
                        type=cli_utils.check_positive_int,
                        help='number of hash functions per band',
                        metavar='num_rows', dest='rows_per_band')
    parser.add_argument('-f', '--force', action='store_true',
                        help='suppress warning messages')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='display progress messages')
    parser.add_argument('-p', '--profile', action='store_true',
                        help='record time profile of search')

    args = parser.parse_args()
    cli_utils.check_invalid_hash_row_values(args)
    parser_warnings(args)

    arg_defaults(args)

    if args.verbose:
        confirm_query(args)

    return args

def confirm_query(args):
    prompt = """
    molecule: %s
    target: %s
    num_cutoff: %i
    sim_cutoff: %.3f
    force: %s
    verbose: %s
    profile: %s
    """ % (args.molecule, args.target, args.num_cutoff, args.sim_cutoff,
           args.force, args.verbose, args.profile)
    print(prompt)

def main():
    args = parse_args()

    lsh_similarity.tanimoto_lsh_search(**vars(args))

if __name__ == '__main__':
    main()
