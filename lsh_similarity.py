import sys
import sqlite3 as sql
import time

import pandas as pd
from rdkit import Chem

import native_shingle
import native_min_hash
import native_lsh
import buckets
import gpu_tanimoto

DEFAULT_MAX_NUMBER_CUTOFF = sys.maxint
DEFAULT_SIMILARITY_CUTOFF = 0
CHUNK_SIZE = 500

SHINGLE_SIZE = native_shingle.SHINGLE_SIZE
NUM_HASH_FUNCTIONS = native_min_hash.NUM_HASH_FUNCTIONS
NUM_SHINGLES = native_min_hash.NUM_SHINGLES
DEFAULT_DB_PATH = buckets.DEFAULT_DB_PATH


def send_to_db(df, conn, sim_func='tanimoto'):
    #rows should be (smiles, RDK5FPhex, shingles, signatures, hashes)
    #what is the key for?
    key = 0
    for row in df.itertuples():
        smi = row[1]
        rdk = row[2]
        pre = '';
        for signature, i in zip(row[5], xrange(len(row[5]))):
            band = i
            value = signature
            #remove buckets from db, hash value = bucket
            bucket = value;

            buckets.insert_one(smi, rdk, value, bucket, key, band, pre, conn)


def precompute_tanimoto_lsh(store, target, verbose=0, profile=False,
                            chunk_size=CHUNK_SIZE,**kwargs):
    """Generates tables in target SQLite db, using hex values from store

    The precomputation goes through three computational steps, shingling,
    min hashing, and locality sensitive hashing. Then uses target to store
    the resulting buckets and precomputation information.

    Args:
        store (str): name of the filestore to read hex values from. The store
            should have pieces in 'data'
        target (str): name of the SQLite db to store the precomputation info in
        verbose (int): level of verbose to use
        kwargs (dict): see native_shingle.py, native_min_hash.py

    TODO: write out what all the important kwargs do

    """
    chem_store = pd.HDFStore(store, 'r')
    hexes = chem_store.select('data', chunksize=chunk_size)
    buckets.create_table(target)
    finished_molecules = buckets.num_finished_molecules(target, **kwargs)

    if profile:
        shingles_times = []
        minhash_times = []
        lsh_times = []
        dbwrite_times = []

    conn = sql.connect(target)
    i = 1
    for chunk in hexes:
        if finished_molecules >= chunk_size:
            finished_molecules -= chunk_size
            i += 1
            continue
        elif finished_molecules > 0:
            chunk = chunk.drop(chunk.head(finished_molecules).index)
            finished_molecules = 0

        #if verbose > 0:
        print('Chunk %s' % i)
        i += 1

        if profile:
            shingle_start = time.time()
        shingles = native_shingle.shingle_df(chunk, verbose=(verbose-1), **kwargs)
        chunk['shingles'] = shingles

        if profile:
            shingle_times.append(time.time() - shingle_start)
            minhash_start = time.time()
        signatures = native_min_hash.min_hash_df(chunk, verbose=(verbose-1),
                                                 **kwargs)
        chunk['signatures'] = signatures

        if profile:
            minhash_times.append(time.time() - minhash_start)
            lsh_start = time.time()
        hashes = native_lsh.native_lsh_df(chunk, verbose=(verbose-1), **kwargs)
        chunk['hashes'] = hashes

        if profile:
            lsh_times.append(time.time() - lsh_start)
            dbwrite_start = time.time
        send_to_db(chunk, conn)
        conn.commit()

        if profile:
            dbwrite_times.append(time.time() - dbwrite_start)
        if verbose > 0:
            print(shingles)
            print(signatures)
            print(hashes)

    if profile:
        times = zip(xrange(len(shingles_times)), shingle_times, minhash_times, lsh_times, dbwrite_times)
        averages = map(lambda x: sum(x) / float(len(x)), zip(*times))
        print('Average time to complete one chunk.')
        print('Avg Shingle Time\tAvg Minhash Time\tAvg LSH Time\tAvg DB Write Time')

    chem_store.close()
    conn.close()

def find_collisions(hashes, target):
    returned_collisions = []
    for h, i in zip(hashes, xrange(len(hashes))):
        returned_collisions.append(buckets.get_SMI(i,h,target))

    candidates = []

    for band in returned_collisions:
        for x in band:
            candidates.append(x)

    return candidates

def tanimoto_lsh_search(molecule, target=DEFAULT_DB_PATH, num_cutoff=DEFAULT_MAX_NUMBER_CUTOFF,
                        sim_cutoff=DEFAULT_SIMILARITY_CUTOFF, verbose=False, profile=False,
                        force=False, **kwargs):
    """Returns a list of molecules sorted by tanimoto similarity to the query.

    The search utilizes precomputed buckets generated through minhashing to
    mimic Jaccard similarity which is equivalent to Tanimoto similarity on bit
    vectors. Additionally, precomputation information is stored with the
    buckets found in the target SQLite DB. Use precompute_tanimoto_lsh() to
    generate buckets for a data store.

    Args:
        molecule (str): Smiles formated string which will be used to find matches
            based on Tanimoto similarity.
        target (str): SQLite db containing the precomputed information used
            to generate results for the query.
        num_cutoff (int): maximum length of the returned list
        sim_cutoff (float): lowest similarity for molecules to be returned on
            the list
        verbose (bool): prints progress to stdout

    Returns:
        dataframe: Sorted dataframe of chemicals that meet the requirements in
            the args. Not guaranteed to be of length num_cutoff
    """
    mol = Chem.MolFromSmiles(molecule)
    fingerprint = Chem.RDKFingerprint(mol, maxPath=5, fpSize=1024).ToBitString()
    if profile:
        shingle_start = time.time()
    shingles =  native_shingle.n_shingle(fingerprint, **kwargs)

    if profile:
        shingle_time = time.time() - shingle_start
        minhash_start = time.time()
    min_hash = native_min_hash.min_hash(shingles, **kwargs)
    #print(fingerprint)

    if profile:
        minhash_time = time.time() - minhash_start
        lsh_start = time.time()
    hashes = native_lsh.native_lsh(min_hash, **kwargs)
    #print(hashes)

    collisions = find_collisions(hashes, target)
    colliding_set = set([collision[:2] for collision in collisions])

    #for collision in collisions:
        #print(collision)
    print('Collisions found: %i' % len(collisions))

    unique_collisions = []
    for collision in colliding_set:
        #print(collision)
        unique_collisions.append(collision)
    print('Unique matches: %i' % len(colliding_set))
    
    if profile:
        lsh_time = time.time() - lsh_start
        gpu_start = time.time()
    
    gpu_tanimoto.GPUtanimoto(fingerprint, unique_collisions, num_cutoff, sim_cutoff)
    if profile:
        gpu_time = time.time() - gpu_start

    if profile:
        print('Shingle time: %f' % shingle_time)
        print('Minhash time: %f' % minhash_time)
        print('LSH time: %f' % lsh_time)
        print('GPU Comparison time: %f' % gpu_time)

def main():
    #precompute_tanimoto_lsh('/data/gpusearch/data/example50K-hex.h5',
    #'data/50k-120hash-buckets.db', verbose=0)
    #print('Done')
    tanimoto_lsh_search('CN1C2C1(C)CC(CN)C2(C)CN', target='data/buckets.db', profile=True, shingle_size=24, num_hash_functions=6, rows_per_band=3)
    #tanimoto_lsh_search('CCN(CCC1(N)CCCC1)C=N')

if __name__ == '__main__':
    main()
