import time

from native_min_hash import NUM_SHINGLES
from native_min_hash import NUM_HASH_FUNCTIONS
from bitstring import BitArray

import numpy as np

ROWS_PER_BAND = 5 # TESTING ONLY change to desired default (5)
NUM_BANDS = 20 # TESTING ONLY change to desired default (20)
SIGNATURES_COLUMN = 'signatures'
NUM_DOCS = 30 # TESTING ONLY, pull from df or other module

def int2bitarray(x):
    return BitArray(uint=x, length=NUM_SHINGLES)

def lsh(sig_matrix):

    # currently, minhash stores rows as int, not possible to test this
    # with np.random.randint on our full lsh step (requires c long convert)f
    assert num_hashes_valid()

    buckets = {}
    x = 0
    for row in xrange(len(sig_matrix)):
        hash_value = BitArray()
        for i in xrange(0, ROWS_PER_BAND):
            # concatentation of the signature is our hash function
            # convert to bitstrings to avoid problems like {1,23,45}
            # and {12,34,5} having same hash value
            # unfortunately this conversion is very slow...

            hash_value += int2bitarray(sig_matrix[row][i])

            # addition based (bad) hash to see if int2bin is the slowdown
            #hash_value += sig_matrix[row][i]
        print 'Doc %i/%i hashed' % (x, NUM_DOCS - 1)
        x += 1
        buckets.setdefault(hash_value.bin, [])
        buckets[hash_value.bin].append(row)
        buckets[hash_value.bin] = set(buckets[hash_value.bin])

    return buckets

def alternate_lsh(sig_matrix):

    assert num_hashes_valid()

    alt_buckets = {}
    x = 0
    for row in xrange(len(sig_matrix)):
        hash_value = 0
        for i in xrange(0, ROWS_PER_BAND):
            # concatentation of the signature is our hash function
            # convert to bitstrings to avoid problems like {1,23,45}
            # and {12,34,5} having same hash value
            # unfortunately this conversion is very slow...

            #hash_value += int2bitarray(sig_matrix[row][i])

            # addition based (bad) hash to see if int2bin is the slowdown
            hash_value += sig_matrix[row][i]
        if (x % 5 == 0):
            print 'Doc %i/%i hashed' % (x, NUM_DOCS - 1)
        x += 1
        alt_buckets.setdefault(hash_value, [])
        alt_buckets[hash_value].append(row)
        alt_buckets[hash_value] = set(alt_buckets[hash_value])
    print 'Doc %i/%i hashed' % (x - 1, NUM_DOCS - 1)
    return buckets


def lsh_df_band(band, **kwargs):
    sig_matrix = [BitArray()] * MAX_SIG_VALUE

    # invert matrix, makes keeping track of document ID easier in next step
    for row in band:
        for x in xrange(0, NUM_DOCS):
            sig_matrix[x].append(band[row][x])
    return lsh(sig_matrix)

def lsh_df(df, **kwargs):
    band_func = lambda band: lsh_df_band(band, **kwargs)
    for band in xrange(NUM_BANDS):
        df['band'+band] = df.ix[band*ROWS_PER_BAND, (band+1)*ROWS_PER_BAND]
        lsh_df_band(df['band'+band], **kwargs)
    return df['buckets']

def chunker(doc, size):
    for x in xrange(0, len(doc), size):
        yield doc[x:x+size]

def native_lsh(signature, rows_per_band=ROWS_PER_BAND, **kwargs):
    hashes = [hash(tuple(chunk)) for chunk in chunker(signature, rows_per_band)]
    return hashes

def native_lsh_row(row, **kwargs):
    return native_lsh(row['signatures'], **kwargs)

def native_lsh_df(df, **kwargs):
    row_func = lambda row: native_lsh_row(row, **kwargs)
    signatures = df.apply(row_func, axis=1)
    return signatures


def jaccard_sim_test(set1, set2, cutoff):
    '''Compute jaccard similarity between halves of a bucket.
        Failure isn't fatal, but you may wish to emit a warning.'''
    sim = len(set1.intersection(set2))/float(max(len(set1),len(set2)))
    return sim <= cutoff

def lsh_performance_test():
    np.random.seed(0)
    start_time = time.time()

    print('\nGenerating a band of test signatures')
    p = 0.1 #testing-probability for shingles matching, real value much lower
    test_sigs = np.random.geometric(p, size=(NUM_DOCS, ROWS_PER_BAND))
    sigs_gen_duration = time.time() - start_time
    print('%.5fs test signature generation time\n' % sigs_gen_duration)

    start_time = time.time()

    print('LSH Type 1 begin:')
    buckets = lsh(test_sigs)
    lsh_duration = time.time() - start_time
    print('%.5fs LSH 1 time\n' % lsh_duration)

    start_time = time.time()

    print('LSH Type 2 begin:')
    alt_buckets = alternate_lsh(test_sigs)
    lsh_duration = time.time() - start_time
    print('%.5fs LSH 2 time\n' % lsh_duration)

    print('%i buckets generated' % len(buckets))
    print('%i candidate pairs produced.' % (NUM_DOCS - len(buckets)))

def num_hashes_valid():
    return ROWS_PER_BAND * NUM_BANDS == NUM_HASH_FUNCTIONS

def main():
    lsh_performance_test()

if __name__ == '__main__':
    main()
