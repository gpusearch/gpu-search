import time

import numpy as np

from native_shingle import SHINGLE_SIZE

NUM_HASH_FUNCTIONS = 120
NUM_SHINGLES = 2 ** SHINGLE_SIZE
SHINGLE_COLUMN = 'shingles'
PERMUTATIONS_FOLDER = 'permutations/'
DEFAULT_PERMUTATION_PATH = 'permutation_matrix.npy'

def num_possible_shingles(shingle_size=SHINGLE_SIZE):
    #Number of all possible shingles assuming the smallest piece is a bit
    return 2 ** shingle_size

def find_permutation_file_path(shingle_size, folder=PERMUTATIONS_FOLDER):
    #points to wherever the correct permutation matrix is at
    return '%s%iS_perm_matrix.npy' % (folder, shingle_size)

def generate_hash_permutations(num_hash_functions=NUM_HASH_FUNCTIONS,
                               shingle_size=SHINGLE_SIZE, verbose=0,
                               **kwargs):
    permutation_path = find_permutation_file_path(shingle_size)
    try:
        with open(permutation_path, 'r') as f:
            permutation_array = np.load(f)
            if len(permutation_array) >= num_hash_functions:
                return array
    except:
        pass

    num_shingles = num_possible_shingles(shingle_size)
    #Ensure the hash permutations generated will be consistant but random
    np.random.seed(0)
    hash_permutations = np.empty((num_hash_functions, num_shingles))

    for x in xrange(num_hash_functions):
        start = time.time()
        #hash_permutations.append(np.random.permutation(num_shingles))
        hash_permutations[x] = np.random.permutation(num_shingles)
        if verbose > 0:
            duration = time.time() - start
            print('%i hash function generated in %.5fs' % (x, duration))

    np.save(permutation_path, hash_permutations)
    return hash_permutations

def find_first(shingles, permutation):
    for x in xrange(len(permutation)):
        if permutation[x] in shingles:
            return x
        else:
            pass
    #Only happens when there isn't a permutation to match every shingle
    raise Exception('No shingle matched')
    return -1


def min_hash(shingles, num_hash_functions=NUM_HASH_FUNCTIONS,
             shingle_size=SHINGLE_SIZE,
             verbose=0, **kwargs):
    num_shingles = num_possible_shingles(shingle_size)

    #Check if the permutations for the hash functions has been specified
    if not hasattr(min_hash, 'hash_permutations'):
        min_hash.hash_permutations = generate_hash_permutations(num_hash_functions, shingle_size, verbose-1)

    #Generate new hash permutations if there aren't enough permutations yet
    if len(min_hash.hash_permutations) < num_hash_functions:
        min_hash.hash_permutations = generate_hash_permutations(num_hash_functions, shingle_size, verbose-1)

    min_values = []
    for x in xrange(num_hash_functions):
        permutation = min_hash.hash_permutations[x]
        if verbose > 0:
            start = time.time()
        min_values.append(find_first(shingles, permutation))
        if verbose > 0:
            duration = time.time() - start
            print("%i hash completed in %.5fs" % (x, duration))
    return min_values

def min_hash_df_row(row, num_hash_functions=NUM_HASH_FUNCTIONS,
                    shingle_column=SHINGLE_COLUMN, **kwargs):
    return min_hash(row[shingle_column], num_hash_functions, **kwargs)

def min_hash_df(df, **kwargs):
    min_hash.hash_permutations = generate_hash_permutations(**kwargs)
    row_func = lambda row: min_hash_df_row(row, **kwargs)
    signatures = df.apply(row_func, axis=1)
    return signatures

def clear_hash_permutations():
    min_hash.hash_permutations = []

def permutation_generation_test():
    permutations = generate_hash_permutations()
    assert len(permutations) >= NUM_HASH_FUNCTIONS

    for perm in permutations:
        set_perm = set(perm)
        for x in xrange(num_possible_shingles(SHINGLE_SIZE)):
            assert x in set_perm

    print('permutation_generation_test: pass')

def min_hash_zeros_test():
    #clear_hash_permutations()

    zero_set = set([0])
    signature = min_hash(zero_set)
    values = [perm.tolist().index(0) for perm in min_hash.hash_permutations]
    assert signature == values

    print('min_hash_zeros_test: pass')

def min_hash_rand_test():
    #clear_hash_permutations()
    np.random.seed(67)

    num_shingles = num_possible_shingles(SHINGLE_SIZE)
    rand_set = set(np.random.randint(0, num_shingles, 1024))
    signature = min_hash(rand_set)
    for x in xrange(len(signature)):
        num = signature[x]
        permutation = min_hash.hash_permutations[x]
        for y in xrange(num):
            assert permutation[y] not in rand_set
        assert permutation[num] in rand_set

    print('min_hash_rand_test: pass')

def min_hash_performance_test():
    #seed of 0 has first shingle index at ~16 million
    #takes a long time
    #np.random.seed(0)
    np.random.seed(67)
    clear_hash_permutations()
    num_shingles = num_possible_shingles(SHINGLE_SIZE)

    start_time = time.time()

    test_array = np.random.randint(0, num_shingles, 1024)
    generation_duration = time.time() - start_time

    start_time = time.time()
    test_set = set(test_array)
    set_duration = time.time() - start_time

    #print('Generation Duration: %.8f' % generation_duration)
    #print('Set Duration: %.8f' % set_duration)

    print('Hash permutation generation:')
    start_time = time.time()
    min_hash.hash_permutations = generate_hash_permutations(verbose=1)
    total_time = time.time() - start_time
    average_time = total_time/NUM_HASH_FUNCTIONS
    print('Generated %i hash permutations in: %.3fs' %
          (NUM_HASH_FUNCTIONS, total_time))
    print('%.5fs average generation time\n' % average_time)

    print('Min hash on list:')
    start_time = time.time()
    array_signature = min_hash(test_array, verbose=10)
    total_time = time.time() - start_time
    average_time = total_time/NUM_HASH_FUNCTIONS
    print('Generated %i min hashes in: %.3fs' %
          (NUM_HASH_FUNCTIONS, total_time))
    print('%.5fs average min hash time\n' % average_time)

    print('Min hash on set:')
    start_time = time.time()
    set_signature = min_hash(test_set, verbose=10)
    total_time = time.time() - start_time
    average_time = total_time/NUM_HASH_FUNCTIONS
    print('Generated %i min hashes in: %.3fs' %
          (NUM_HASH_FUNCTIONS, total_time))
    print('%.5fs average min hash time\n' % average_time)

    assert array_signature == set_signature
    print(array_signature)


def main():
    #permutation_generation_test()
    #min_hash_zeros_test()
    #min_hash_rand_test()
    min_hash_performance_test()

    #hash_permutations = generate_hash_permutations()
    #array = np.array(hash_permutations)
    #with open(DEFAULT_PERMUTATION_PATH, 'w+') as f:
    #    np.save(f, array)
    #print(array)
    #with open(DEFAULT_PERMUTATION_PATH, 'r') as f:
    #    a = np.load(f)
    #print(a)
    #
    #array = np.empty((2,3))
    #array[0] = np.random.permutation(3)
    #print(len(array))



if __name__ == '__main__':
    main()
