import time
import sys

import pandas as pd
import numpy as np
import bitstring
import itertools
import binascii
from rdkit import Chem, DataStructs
from bitstring import BitArray

#define chunk size
CHUNK_SIZE = 1000

#define shingle size
SHINGLE_SIZE = 20

FP_COLUMN = 'RDK5FPhex'

def bin2int32(x):
    return np.uint32(BitArray(bin=x).uint)

def int2bin(x):
    return BitArray(uint=x, length=SHINGLE_SIZE).bin

def n_shingle(document, shingle_size=SHINGLE_SIZE, unique=True, **kwargs):
    n = shingle_size
    num_parts = len(document) - n + 1
    output = [bin2int32(document[i:i+n]) for i in xrange(num_parts)]

    #used for testing code realistically
    if unique:
        output = set(output)

    return output

def validate_shingles(original_doc, shingles):
    shingle = int2bin(shingles[0])
    recombined_doc = shingle[:-1]
    for shingle in shingles:
        recombined_doc += int2bin(shingle)[-1:]
    assert original_doc == recombined_doc

def shingle_df_row(row, shingle_size=SHINGLE_SIZE, fp_column=FP_COLUMN,
                   **kwargs):
    bits = BitArray(hex=row[fp_column]).bin
    shingle = n_shingle(bits, shingle_size=shingle_size)
    return shingle

def shingle_df(df, **kwargs):
    row_func = lambda row: shingle_df_row(row, **kwargs)
    shingles = df.apply(row_func, axis=1)
    return shingles

def main():
    store = pd.HDFStore('/data/gpusearch/data/example50K-hex.h5', 'r')

    hexes = store.select('data', chunksize=CHUNK_SIZE)
    #docs = []

    i = 0
    for chunk in hexes:
        print('Chunk %s:' % i)
        shingles = shingle_df(chunk)
        print(shingles)

        #for doc in shingles.tolist():
        #docs.append(doc)
        i+=1

        #print(docs)

if __name__ == '__main__':
    main()
